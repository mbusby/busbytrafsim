import pygame
from pygame.locals import *
import sys
import png, array, time
import random

#RGB to hex colour coding
def rgb_to_hex(rgb):
    return '%02x%02x%02x' % rgb

trafficLights = []
trafficSpawners = []
spawnerDict = {'5A5C30':'L', '484A16':'R', '8C8C84':'U', '444532':'D'}

mapImage = 'map-circularLiving.png'
global nameCount
nameCount = 1

#Generates the map from an image file
def generateMap():
    reader = png.Reader(filename=mapImage)
    r = reader.read()
    global mapWidth, mapHeight
    mapWidth = r[0]
    mapHeight = r[1]
    l = list(r[2])
    mapp_RGB = []
    for row in l:
        i = 1
        row_RGB = []
        for value in row:
            if i==1:
                li = []
                li.append(value)
                i += 1 
            elif i==2:
                li.append(value)
                i += 1
            elif i==3:
                li.append(value)
                row_RGB.append(rgb_to_hex(tuple(li)))
                i=1
        mapp_RGB.append(row_RGB)

    mapp = []
    i = 0
    for row in mapp_RGB:
        c = 0
        rowConverted = []
        for pixel in row:
            pixel = pixel.upper()
            #look up pixel colour to determine direction
            if pixel=="FFFFFF":
                rowConverted.append('N')
            elif pixel=="000000":
                #Inherited tile, gives the same as the last direction
                rowConverted.append('I')
            elif pixel=="CA6EFF":
                rowConverted.append('R')
            elif pixel=="FF0000":
                rowConverted.append('L')
            elif pixel=="00FFFF":
                rowConverted.append('U')
            elif pixel=="00FF08":
                rowConverted.append('D')
            elif pixel=="E8F00E":
                #Creates traffic light instance around the point c,i
                trafficLights.append(trafficLight((c,i), 10))
                rowConverted.append('T')
            elif pixel=="B5BA13":
                #Creates regular traffic light tile, similar to inherit in behaviour
                rowConverted.append('T')
            elif pixel in spawnerDict:
                #Creates a traffic spawner at the location of the direction as described
                rowConverted.append(spawnerDict[pixel])
                trafficSpawners.append([i,c])
            c += 1
        i += 1
        mapp.append(rowConverted)
    return mapp


class trafficLight():
    def __init__(self, point, period):
        self.points = [point, (point[0]+1, point[1]+1), (point[0], point[1]+1), (point[0]+1, point[1])]
        self.lr = True
        self.ud = False
        self.period = period
        self.amber = False
        self.c=0
    def turn(self):
        if counter%self.period==0:
            self.amber=True
        else:
            if self.amber:
                if self.noCars():
                    self.flipLights()
                    self.amber=False
    def flipLights(self):
        #print "Lights changed!"
        self.lr, self.ud = self.ud, self.lr
    def noCars(self):
        for car in cars:
            if (car.x, car.y) in self.points:
                return False
        return True
    

        
class Car():
    def __init__(self, y, x):
        global nameCount
        self.name = '#%s' % (nameCount)
        nameCount += 1
        self.x = x
        self.y = y
        self.direction = mapp[self.y][self.x]
        self.lastdirection = self.direction
        self.goingThroughLights = False
        self.turning = False
        self.origin = 'has not turned'
        turnt = 2 #That's so turnt

    def drawCar(self):
        pygame.draw.rect(screen, (0,0,0), (self.x*4, self.y*4, 4, 4), 0)
        #pygame.draw.rect(screen, (0,0,0), (self.x, self.y, 2, 2), 0)
    def moveCar(self):
        if self.turning and not self.collisionPossible():
            #Use directions from turning
            self.direction = self.turningList[0]
            self.goingThroughLights
            if len(self.turningList)<2:
                #Stop turning the car
                self.turning = False
            self.turningList.pop(0)
            self.setDirection()
        elif self.turning:
            #Don't move because a car is there, or the car is are at a traffic light
            self.direction = 'N'
        else:
            #Use directions from square underneath
            self.direction = mapp[self.y][self.x]
            if self.direction == 'I' or self.direction == 'T':
                self.direction = self.lastdirection
                self.goingThroughLights = True
            else:
                self.goingThroughLights = False
            self.setDirection()
            self.lastdirection = self.direction
            if mapp[self.y+self.vy][self.x+self.vx]=='T' and not self.goingThroughLights and not self.collisionPossible():
                if random.randint(1,3)==1:
                    self.turn(random.choice(['L','R']))
                    self.turning = True
                    self.goingThroughLights = True
        if self.collisionPossible() or self.direction=='N':
            #Don't let the car move
            pass
        else:
            #Let the car move
            self.x += self.vx
            self.y += self.vy
        self.drawCar()

    def setDirection(self):
            #Sets the relevant velocity based on the direction the car is to go in
            if self.direction == 'L':
                self.vx = -1
                self.vy = 0
            elif self.direction == 'R':
                self.vx = 1
                self.vy = 0
            elif self.direction == 'U':
                self.vx = 0
                self.vy = -1
            elif self.direction == 'D':
                self.vx = 0
                self.vy = 1
    def collisionPossible(self):
        if 0 < self.x+self.vx < mapWidth-1 and 0 < self.y+self.vy < mapHeight-1:
            if self.noLights():
                carsTemp = cars[:]
                carsTemp.remove(self)
                for car in carsTemp:
                    if car.x == self.x+self.vx and car.y == self.y+self.vy:
                        #print "Woah almost accident between %s and %s!" % (car.name, self.name)
                        return True
                    elif car.x==self.x and car.y==self.y:
                        #print "Crash between %s and %s!" % (car.name, self.name)
                        self.reportCrash(car.name)
                return False
            else:
                return True
        else:
            #Car no longer on the map at all
            cars.remove(self)
            return False
    def noLights(self):
        #checks traffic lights
        for light in trafficLights:
            if self.goingThroughLights:
                return True
            if (self.x+self.vx, self.y+self.vy) in light.points:
                if not light.amber:
                    if self.direction == 'L' or self.direction=='R':
                        if light.ud:
                            return False
                    else:
                        if light.lr:
                            return False
                    self.goingThroughLights = True
                else:
                    if self.goingThroughLights:
                        return True
                    else:
                        return False
        return True
        
    def reportCrash(self, carName):
        with open("report.txt", "a") as reportFile:
            reportFile.write("%s crashed into %s at turn %s" % (self.name, carName, counter))
    def turn(self, direction):
        if not self.turning:
            if self.direction=='L' and direction=='L':
                self.turningList = ['D', 'D', 'D']
                self.origin = 'Left'
            elif self.direction=='L' and direction=='R':
                self.turningList = ['L', 'U', 'U', 'U']
                self.origin = 'Left'
            elif self.direction=='R' and direction=='L':
                self.origin = 'Right'
                self.turningList = ['U', 'U', 'U']
            elif self.direction=='R' and direction=='R':
                self.origin = 'Right'
                self.turningList = ['R', 'D', 'D', 'D']
            elif self.direction=='U' and direction=='L':
                self.turningList = ['L', 'L', 'L']
                self.origin = 'Up'
            elif self.direction=='U' and direction=='R':
                self.turningList = ['U', 'R', 'R', 'R']
                self.origin = 'Up'
            elif self.direction=='D' and direction=='L':
                self.turningList = ['R', 'R', 'R']
                self.origin = 'Down'
            elif self.direction=='D' and direction=='R':
                self.turningList = ['D', 'L', 'L', 'L']
                self.origin = 'Down'
            #print "Car %s turned!" % self.name
            self.i = 0
            

        
def spawnCar(points):
    if random.randint(1,2)==1:
        cars.append(Car(points[0],points[1]))
    #print "Car created at tile %s" % mapp[points[0]][points[1]]
def populate():
    for spawnerPoint in trafficSpawners:
        spawnCar(spawnerPoint)

mapp = generateMap()
width = mapHeight*4
height = mapWidth*4
pygame.init()
fps = 20
screen = pygame.display.set_mode((width,height))
clock = pygame.time.Clock()
counter = 0
bg = pygame.image.load(mapImage)
bg = pygame.transform.scale(bg, (width, height))

cars = []
populate()
while True:
    counter += 1
    screen.fill((255,255,255))
    screen.blit(bg, (0,0))
    for event in pygame.event.get():
        #Quits the game
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    if pygame.mouse.get_pressed()[0]:
        mouseLocation=pygame.mouse.get_pos()
        for car in cars:
            if car.x==int(mouseLocation[0]/4) and car.y==int(mouseLocation[1]/4):
                print "The car you clicked is now driving in a %s direction, was last in a %s direction, turning is %s and is at location %s,%s, origin is %s" % (car.direction, car.lastdirection, car.turning, car.x, car.y, car.origin)
    if pygame.key.get_pressed()[pygame.K_d]:
        fps += 5
        print 'Added 5 onto fps, fps now at %s' % fps
    if pygame.key.get_pressed()[pygame.K_s]:
        fps -= 5
        print 'Added 5 onto fps, fps now at %s' % fps
    if pygame.key.get_pressed()[pygame.K_p]:
        fps = 0      
        print 'Paused!'      
    if counter%10==0:
        populate()
    if counter%5==0:
        spawnCar(trafficSpawners[0])
    for light in trafficLights:
        light.turn()
    for car in cars:
        car.moveCar()
    pygame.display.flip()
    clock.tick(fps)
pygame.quit()
sys.exit()
